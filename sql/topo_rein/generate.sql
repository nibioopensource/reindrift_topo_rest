----------------------------------
-- Define common json blocks
----------------------------------
--{

--{ common tables
--  FIXME: should be TYPES, but topo_update APPCONFIG does not
--         support types at the time of writing
--
SELECT $$
	{
		"name": "sosi_registreringsversjon",
		"attributes": [
			{
				"name": "produkt",
				"type": "varchar"
			},
			{
				"name": "versjon",
				"type": "varchar"
			}
		]
	},
	{
		"name": "sosi_kvalitet",
		"attributes": [
			{
				"name": "maalemetode",
				"type": "smallint"
			},
			{
				"name": "noyaktighet",
				"type": "integer"
			},
			{
				"name": "synbarhet",
				"type": "smallint"
			}
		]
	},
	{
		"name": "sosi_felles_egenskaper",
		"attributes": [
			{
				"name": "forstedatafangstdato",
				"type": "date"
			},
			{
				"name": "identifikasjon",
				"type": "varchar"
			},
			{
				"name": "kvalitet",
				"type": "sosi_kvalitet"
			},
			{
				"name": "oppdateringsdato",
				"type": "date"
			},
			{
				"name": "opphav",
				"type": "varchar",
				"modifiers": [255]
			},
			{
				"name": "verifiseringsdato",
				"type": "date"
			},
			{
				"name": "informasjon",
				"type": "varchar",
				"modifiers": [255]
			},
			{
				"name": "registreringsversjon",
				"type": "sosi_registreringsversjon"
			}
		]
	}
$$ as common_tables \gset
--}

--{ common domains
SELECT $$
	{
		"name": "reinbeitebruker_id",
		"type": "varchar",
		"modifiers": [ "3" ],
		"allowed_values": [
			"XI", "ZA", "ZB", "ZC", "ZD", "ZE", "ZF",
			"ØG", "UW", "UX", "UY", "UZ", "ØA", "ØB",
			"ØC", "ØE", "ØF", "ZG", "ZH", "ZJ", "ZS",
			"ZL", "ZÅ", "YA", "YB", "YC", "YD", "YE",
			"YF", "YG", "XM", "XR", "XT", "YH", "YI",
			"YJ", "YK", "YL", "YM", "YN", "YP", "YX",
			"YR", "YS", "YT", "YU", "YV", "YW", "YY",
			"XA", "XD", "XE", "XG", "XH", "XJ", "XK",
			"XL", "XM", "XR", "XS", "XT", "XN", "XØ",
			"XP", "XU", "XV", "XW", "XZ", "XX", "XY",
			"WA", "WB", "WD", "WF", "WK", "WL", "WN",
			"WP", "WR", "WS", "WX", "WZ", "VA", "VF",
			"VG", "VJ", "VM", "VR", "YQA", "YQB",
			"YQC", "ZZ", "RR", "ZQA"
		]
	}
$$ as common_domains \gset
--}

--}
----------------------------------
-- Declaratively generate schemas
----------------------------------
--{

-- Generate rko (surface and border)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "konvensjonsomrade_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "konvensjonsomrade_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "samebyid1",
							"type": "varchar",
							"modifiers": [ 2 ]
						},
						{
							"name": "samebyid2",
							"type": "varchar",
							"modifiers": [ 2 ]
						},
						{
							"name": "informasjon",
							"type": "varchar",
							"modifiers": [ 254 ]
						},
						{
							"name": "samebykontakt",
							"type": "varchar",
							"modifiers": [ 100 ]
						},
						{
							"name": "eier",
							"type": "varchar",
							"modifiers": [ 254 ]
						},
						{
							"name": "status",
							"type": "integer"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "reinbeitebruker_id",
							"type": "varchar",
							"modifiers": [ 3 ]
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "konvensjonsomrade_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "konvensjonsomrade_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rko')
) v(suffix);
--}

-- Generate ran (line and point layers)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "reindrift_anlegg_linje",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "reinbeitebruker_id2",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "reinbeitebruker_id3",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "alle_reinbeitebr_id",
							"type": "varchar",
							/* TODO: not null */
							"default": ""
						},
						{
							/*
							-- This is flag used indicate the status of this record. The rules for how to use this flag is not decided yet.
							-- Here is a list of the current states.
							-- 0: Ukjent (uknown)
							-- 1: Godkjent
							-- 10: Endret
							*/
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "anleggstype",
							"type": "integer",
							"allowed_values": [ 1, 2, 3, 4, 5, 6, 7, 12 ]
						},
						{
							"name": "id_copy",
							"type": "integer"
						}
					],
					"topogeom_columns": [
						{
							"name": "linje",
							"type": "lineal"
						}
					]
				},
				{
					"name": "reindrift_anlegg_punkt",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "reinbeitebruker_id2",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "reinbeitebruker_id3",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "alle_reinbeitebr_id",
							"type": "varchar",
							/* TODO: not null */
							"default": ""
						},
						{
							/*
							-- This is flag used indicate the status of this record. The rules for how to use this flag is not decided yet.
							-- Here is a list of the current states.
							-- 0: Ukjent (uknown)
							-- 1: Godkjent
							-- 10: Endret
							*/
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "anleggstype",
							"type": "integer",
							"allowed_values": [ 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ]
						},
						{
							"name": "id_copy",
							"type": "integer"
						}
					],
					"topogeom_columns": [
						{
							"name": "punkt",
							"type": "puntal"
						}
					]
				}
			],
			"path_layer": {
				"table_name": "reindrift_anlegg_linje",
				"geo_column": "linje"
			},
			"point_layer": {
				"table_name": "reindrift_anlegg_punkt",
				"geo_column": "punkt"
			},
			"operations": {
				"AddPoint": { },
				"RemovePoint": { },
				"MovePoint": { },
				"AddPath": { },
				"RemovePath": { },
				"SplitPaths": { }
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('ran')
) v(suffix);
--}

-- Generate rvr (surface and border)
-- {
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "arstidsbeite_var_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "arstidsbeite_var_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "alle_reinbeitebr_id",
							"type": "varchar",
							/* TODO: not null */
							"default": ""
						},
						{
							/*
							-- This is flag used indicate the status of this record. The rules for how to use this flag is not decided yet.
							-- Here is a list of the current states.
							-- 0: Ukjent (uknown)
							-- 1: Godkjent
							-- 10: Endret
							*/
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "reindrift_sesongomrade_kode",
							"type": "integer",
							/* WAS: CHECK ( reindrift_sesongomrade_kode > 0 AND reindrift_sesongomrade_kode < 3) */
							"allowed_values": [ 1, 2 ]
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "arstidsbeite_var_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "arstidsbeite_var_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rvr')
) v(suffix);
--}

-- Generate rso (surface and border)
-- {
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "arstidsbeite_sommer_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "arstidsbeite_sommer_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "alle_reinbeitebr_id",
							"type": "varchar",
							/* TODO: not null */
							"default": ""
						},
						{
							/*
							-- This is flag used indicate the status of this record. The rules for how to use this flag is not decided yet.
							-- Here is a list of the current states.
							-- 0: Ukjent (uknown)
							-- 1: Godkjent
							-- 10: Endret
							*/
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "reindrift_sesongomrade_kode",
							"type": "integer",
							/* WAS: CHECK ( reindrift_sesongomrade_kode > 0 AND reindrift_sesongomrade_kode < 3) */
							"allowed_values": [ 1, 2 ]
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "arstidsbeite_sommer_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "arstidsbeite_sommer_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rso')
) v(suffix);
--}

-- Generate rhs (surface and border)
-- {
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "arstidsbeite_host_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "arstidsbeite_host_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "alle_reinbeitebr_id",
							"type": "varchar",
							/* TODO: not null */
							"default": ""
						},
						{
							/*
							-- This is flag used indicate the status of this record. The rules for how to use this flag is not decided yet.
							-- Here is a list of the current states.
							-- 0: Ukjent (uknown)
							-- 1: Godkjent
							-- 10: Endret
							*/
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "reindrift_sesongomrade_kode",
							"type": "integer",
							/* WAS: CHECK ( reindrift_sesongomrade_kode > 0 AND reindrift_sesongomrade_kode < 3) */
							"allowed_values": [ 1, 2 ]
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "arstidsbeite_host_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "arstidsbeite_host_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rhs')
) v(suffix);
--}

-- Generate rhv (surface and border)
-- {
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "arstidsbeite_hostvinter_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "arstidsbeite_hostvinter_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "alle_reinbeitebr_id",
							"type": "varchar",
							/* TODO: not null */
							"default": ""
						},
						{
							/*
							-- This is flag used indicate the status of this record. The rules for how to use this flag is not decided yet.
							-- Here is a list of the current states.
							-- 0: Ukjent (uknown)
							-- 1: Godkjent
							-- 10: Endret
							*/
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "reindrift_sesongomrade_kode",
							"type": "integer",
							/* WAS: CHECK ( reindrift_sesongomrade_kode > 0 AND reindrift_sesongomrade_kode < 3) */
							"allowed_values": [ 1, 2 ]
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "arstidsbeite_hostvinter_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "arstidsbeite_hostvinter_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rhv')
) v(suffix);
--}

-- Generate rvi (surface and border)
-- {
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "arstidsbeite_vinter_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "arstidsbeite_vinter_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "alle_reinbeitebr_id",
							"type": "varchar",
							/* TODO: not null */
							"default": ""
						},
						{
							/*
							-- This is flag used indicate the status of this record. The rules for how to use this flag is not decided yet.
							-- Here is a list of the current states.
							-- 0: Ukjent (uknown)
							-- 1: Godkjent
							-- 10: Endret
							*/
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "reindrift_sesongomrade_kode",
							"type": "integer",
							/* WAS: CHECK ( reindrift_sesongomrade_kode > 0 AND reindrift_sesongomrade_kode < 3) */
							"allowed_values": [ 1, 2 ]
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "arstidsbeite_vinter_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "arstidsbeite_vinter_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rvi')
) v(suffix);
--}

-- Generate rtr (line only layer)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "rein_trekklei_linje",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							/*
							-- This is flag used indicate the status of this record. The rules for how to use this flag is not decided yet.
							-- Here is a list of the current states.
							-- 0: Ukjent (uknown)
							-- 1: Godkjent
							-- 10: Endret
							*/
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						}
					],
					"topogeom_columns": [
						{
							"name": "linje",
							"type": "lineal"
						}
					]
				}
			],
			"path_layer": {
				"table_name": "rein_trekklei_linje",
				"geo_column": "linje"
			},
			"operations": {
				"AddPath": { },
				"RemovePath": { },
				"SplitPaths": { }
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rtr')
) v(suffix);
--}

-- Generate rbh (surface and border)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "beitehage_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "beitehage_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "alle_reinbeitebr_id",
							"type": "varchar",
							/* TODO: not null */
							"default": ""
						},
						{
							/*
							-- This is flag used indicate the status of this record. The rules for how to use this flag is not decided yet.
							-- Here is a list of the current states.
							-- 0: Ukjent (uknown)
							-- 1: Godkjent
							-- 10: Endret
							*/
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "reindriftsanleggstype",
							"type": "integer",
							/* TODO: not null */
							"allowed_values": [ 3 ]
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "beitehage_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "beitehage_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rbh')
) v(suffix);
--}

-- Generate rop (surface and border)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "oppsamlingsomrade_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "oppsamlingsomrade_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "reinbeitebruker_id2",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "reinbeitebruker_id3",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "oppsamlingsomrade_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "oppsamlingsomrade_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rop')
) v(suffix);
--}

-- Generate rdr (surface and border)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "flyttlei_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "flyttlei_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "reinbeitebruker_id2",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "reinbeitebruker_id3",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "flyttlei_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "flyttlei_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rdr')
) v(suffix);
--}

-- Generate reb (surface, border and puntual layers)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "reinbeiteomrade_punkt",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeiteområde_id",
							"type": "varchar",
							"modifiers": [ 3 ],
							"allowed_values": [
								"U", "V", "W", "X", "Y", "Z"
							]
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "punkt",
							"type": "puntal"
						}
					]
				},
				{
					"name": "reinbeiteomrade_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "reinbeiteomrade_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeiteområde_id",
							"type": "varchar",
							"modifiers": [ 3 ],
							"allowed_values": [
								"U", "V", "W", "X", "Y", "Z"
							]
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "reinbeiteomrade_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "reinbeiteomrade_grense",
				"geo_column": "grense"
			},
			"point_layer": {
				"table_name": "reinbeiteomrade_punkt",
				"geo_column": "punkt"
			},
			"operations": {
				"AddBordersSplitSurfaces": {},
				"AddPoint": { },
				"RemovePoint": { },
				"MovePoint": { }
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('reb')
) v(suffix);
--}

-- Generate rsi (surface and border)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "siidaomrade_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "siidaomrade_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "navn",
							"type": "varchar",
							/* TODO: not null */
							"default": ""
						},
						{
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "siidaomrade_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "siidaomrade_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rsi')
) v(suffix);
--}

-- Generate rdg (surface and border)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "reinbeitedistrikt_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "reinbeitedistrikt_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "varchar",
							"modifiers": [ 3 ]
						},
						{
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						},
						{
							"name": "id_copy",
							"type": "integer"
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "reinbeitedistrikt_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "reinbeitedistrikt_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rdg')
) v(suffix);
--}

-- Generate reo (surface and border)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "ekspropriasjonsomrade_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "ekspropriasjonsomrade_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "kongeligresolusjon",
							"type": "date"
						},
						{
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "ekspropriasjonsomrade_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "ekspropriasjonsomrade_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('reo')
) v(suffix);
--}

-- Generate rks (surface and border)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "konsesjonsomrade_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "konsesjonsomrade_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "konsesjonsomrade_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "konsesjonsomrade_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rks')
) v(suffix);
--}

-- Generate rav (surface and border)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "avtaleomrade_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "avtaleomrade_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						},
						{
							"name": "avtaletype",
							"type": "varchar",
							"modifiers": [ 2 ]
						},
						{
							/*
							-- This is flag used indicate the status of this record. The rules for how to use this flag is not decided yet.
							-- Here is a list of the current states.
							-- 0: Ukjent (uknown)
							-- 1: Godkjent
							-- 10: Endret
							*/
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						},
						{
							"name": "simple_geo",
							"type": "geometry",
							"modifiers": [ "MultiPolygon", "4258" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "avtaleomrade_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "avtaleomrade_grense",
				"geo_column": "grense"
			},
			"operations": {
				"AddBordersSplitSurfaces": {}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rav')
) v(suffix);
--}

-- Generate rro (surface and border)
--{
SELECT topo_update.app_CreateSchema(
	'topo_rein_' || suffix,
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				"snap_tolerance": 1e-10
			},
			"extensions": [
				"uuid-ossp"
			],
			"domains": [
	$$ || :'common_domains' || $$
			],
			"tables": [
	$$ || :'common_tables' || $$,
				{
					"name": "restriksjonsomrade_grense",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "dom_instans",
							"type": "varchar",
							"modifiers": [ 2 ]
						},
						{
							"name": "dom_dato",
							"type": "date"
						},
						{
							"name": "dom_i_kraft",
							"type": "date"
						},
						{
							"name": "informasjon",
							"type": "text"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal"
						}
					]
				},
				{
					"name": "restriksjonsomrade_flate",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "dom_instans",
							"type": "varchar",
							"modifiers": [ 2 ]
						},
						{
							"name": "dom_dato",
							"type": "date"
						},
						{
							"name": "dom_i_kraft",
							"type": "date"
						},
						{
							"name": "informasjon",
							"type": "text"
						},
						{
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "reinbeitebruker_id",
							"type": "reinbeitebruker_id"
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal"
						}
					]
				},
				{
					"name": "restriksjonsomrade_linje",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "dom_instans",
							"type": "varchar",
							"modifiers": [ 2 ]
						},
						{
							"name": "dom_dato",
							"type": "date"
						},
						{
							"name": "dom_i_kraft",
							"type": "date"
						},
						{
							"name": "status",
							"type": "integer",
							/* TODO: not null */
							"default": "0"
						},
						{
							"name": "informasjon",
							"type": "text"
						},
						{
							"name": "felles_egenskaper",
							"type": "sosi_felles_egenskaper"
						},
						{
							"name": "saksbehandler",
							"type": "varchar"
						},
						{
							"name": "slette_status_kode",
							"type": "smallint",
							/* TODO: not null */
							"default": "0",
							"allowed_values": [ 1, 0 ]
						}
					],
					"topogeom_columns": [
						{
							"name": "linje",
							"type": "lineal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "restriksjonsomrade_flate",
				"geo_column": "omrade"
			},
			"border_layer": {
				"table_name": "restriksjonsomrade_grense",
				"geo_column": "grense"
			},
			"path_layer": {
				"table_name": "restriksjonsomrade_linje",
				"geo_column": "linje"
			},
			"operations": {
				"AddBordersSplitSurfaces": { },
				"AddPath": { },
				"RemovePath": { },
				"SplitPaths": { }
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
)
FROM ( VALUES
	('rro')
) v(suffix);
--}

--}
---------
-- END
---------
