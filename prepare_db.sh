#!/bin/sh

DB="$1"

test -n "${DB}" || {
	echo "Usage: $0 <dbname>" >&2
	exit 1
}

topo_update.sh ${DB} || exit 1

cd $(dirname $0)
psql \
	${DB} \
	--set ON_ERROR_STOP=1 \
	--set VERBOSITY=terse \
	-c 'set client_min_messages to "error"' \
	-Xqf sql/topo_rein/generate.sql \
	> /dev/null
