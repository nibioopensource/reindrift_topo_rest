# Reindrift Topo Update

This a project that depends on

	https://gitlab.com/nibioopensource/pgtopo_update_sql

to set up backend service to support edit of layers for reindrift.


# Database setup

1. Create a database (we'll use "rein" as an example db name)

		createdb rein

2. Load `topo_update` support schema in the database:

		topo_update.sh rein

3. Load the reindrift schema in the database:

		psql -f sql/topo_rein/generate.sql rein

NOTE: you can replace steps 2 and 3 with:

		./prepare_db.sh rein
